extends Button

@onready var ip = get_tree().get_first_node_in_group("ip_address").text
var port = "8060"
var headers = [ "Content-Type: application/x-www-form-urlencoded" ]
@export var cmd = "powerOn"

func _ready():

	pass
	
func exec():
	ip = get_tree().get_first_node_in_group("ip_address").text

	if cmd == "preset_01":
		preset_01()
	elif cmd == "preset_02":
		preset_02()	
	elif cmd == "preset_03":
		preset_03()	
	else:
		var url = str("http://"+ip+":"+port+"/keypress/"+cmd)
		print(url)
		var response = $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
		print(response)
	
func preset_01():
	var url = str("http://"+ip+":"+port+"/keypress/home")
	var response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	
	await get_tree().create_timer(2.0).timeout
	url = str("http://"+ip+":"+port+"/keypress/right")
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	
	await get_tree().create_timer(1.0).timeout
	url = str("http://"+ip+":"+port+"/keypress/select")
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	
func preset_02():
	var url = str("http://"+ip+":"+port+"/keypress/home")
	var response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	
	await get_tree().create_timer(2.0).timeout
	url = str("http://"+ip+":"+port+"/keypress/right")
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	await get_tree().create_timer(0.5).timeout
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	
	await get_tree().create_timer(1.0).timeout
	url = str("http://"+ip+":"+port+"/keypress/select")
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)

func preset_03():
	var url = str("http://"+ip+":"+port+"/keypress/home")
	var response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	
	await get_tree().create_timer(2.0).timeout
	url = str("http://"+ip+":"+port+"/keypress/right")
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	await get_tree().create_timer(0.5).timeout
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	await get_tree().create_timer(0.5).timeout
	response = await $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)
	
	await get_tree().create_timer(1.0).timeout
	url = str("http://"+ip+":"+port+"/keypress/select")
	response = $HTTPRequest.request(url, headers, HTTPClient.METHOD_POST)

